package org.traccar.notificators;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traccar.Context;
import org.traccar.config.Keys;
import org.traccar.model.Event;
import org.traccar.model.Position;
import org.traccar.model.User;
import org.traccar.notification.MessageException;
import org.traccar.notification.NotificationFormatter;
import org.traccar.notification.NotificationMessage;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;

public class Notificator2LabsWhatsappGw extends Notificator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Notificator2LabsWhatsappGw.class);

    private final String urlSendText;
    private final String urlSendLocation;
    private final boolean sendLocation;

    public static class TextMessage {
        @JsonProperty("to")
        private String phone;
        @JsonProperty("message")
        private String text;
    }

    public static class LocationMessage {
        @JsonProperty("to")
        private String phone;
        @JsonProperty("latitude")
        private double latitude;
        @JsonProperty("longitude")
        private double longitude;
        @JsonProperty("description")
        private String description;
    }

    public Notificator2LabsWhatsappGw() {

        urlSendText = String.format(
                "https://wagw.service.dchost.com.br/api/%s/sendMessage",
                Context.getConfig().getString(Keys.NOTIFICATOR_WAGW_KEY));
        urlSendLocation = String.format(
                "https://wagw.service.dchost.com.br/api/%s/sendLocation",
                Context.getConfig().getString(Keys.NOTIFICATOR_WAGW_KEY));
        sendLocation = Context.getConfig().getBoolean(Keys.NOTIFICATOR_WAGW_SEND_LOCATION);

    }

    private void executeRequest(String url, Object message) {
        Context.getClient().target(url).request()
                .async().post(Entity.json(message), new InvocationCallback<Object>() {
                    @Override
                    public void completed(Object o) {
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        LOGGER.warn("Telegram API error", throwable);
                    }
                });
    }

    private LocationMessage createLocationMessage(String phone, Position position) {
        LocationMessage locationMessage = new LocationMessage();
        locationMessage.phone = phone;
        locationMessage.latitude = position.getLatitude();
        locationMessage.longitude = position.getLongitude();
        locationMessage.description = null;
        return locationMessage;
    }

    @Override
    public void sendSync(long userId, Event event, Position position) {

        User user = Context.getPermissionsManager().getUser(userId);
        NotificationMessage shortMessage = NotificationFormatter.formatMessage(userId, event, position, "short");

        TextMessage message = new TextMessage();
        message.phone = user.getString("WaGbPhone");
        message.text = shortMessage.getBody();
        executeRequest(urlSendText, message);
        if (sendLocation && position != null) {
            executeRequest(urlSendLocation, createLocationMessage(message.phone, position));
        }

    }

    @Override
    public void sendAsync(long userId, Event event, Position position) {
        sendSync(userId, event, position);
    }
}
